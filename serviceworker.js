var fileToCache = [
    '/',
    '/static/css/bootstrap.css',
    '/static/css/flexboxgrid.css',
    '/static/css/lightbox.css',
    '/static/css/estilo.css',
    '/static/css/header.css',
    '/static/css/footer.css',
    '/static/css/style.css',

    '/static/img/carrusel/img1.jpg',
    '/static/img/carrusel/img2.jpg',
    '/static/img/empresa/logo_kefra.png',

    '/static/js/app.js',
];

var CACHE_NAME = 'lista';

self.addEventListener('install', function (event) {
    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                console.log('Cache open!');
                return cache.addAll(fileToCache);
            })
    );
});


self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.match(event.request)
            .then(function (response) {
                // Cache hit - return response
                if (response) {
                    return response;
                }
                return fetch(event.request);
            })
    );
});