from django.conf.urls import url, include
from rest_framework import routers
from kafra_app import views
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

router = routers.DefaultRouter()
router.register(r'personas', views.PersonaViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [    
    path(r'', include('kafra_app.urls')), 
    url(r'^admin/', admin.site.urls),    
    url(r'^api/', include(router.urls)),
    path('', include('pwa.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^oauth/', include('social_django.urls', namespace='social')), 
]