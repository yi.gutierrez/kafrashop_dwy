var staticCacheName = 'djangopwa-v1';
var urlsToCache = [
  '/',
  '/static/css/bootstrap.min.css',
  '/static/js/app.js',
  '/static/js/popper.js',
  '/static/js/bootstrap.js',
  '/static/css/bootstrap.css',
  '/static/css/flexboxgrid.css',
  '/static/css/lightbox.css',
  '/static/css/estilo.css',
  '/static/css/footer.css',
  '/static/css/style.css',
  '/static/img/empresa/crowfunding.jpg',
  '/static/img/empresa/logo.png',
  '/static/img/empresa/perro.png',
  '/static/img/empresa/rescate.jpg',
  '/static/img/adoptados/Apolo.jpg',
  '/static/img/adoptados/Duque.jpg',
  '/static/img/adoptados/Tom.jpg',
  '/static/img/rescatados/Bigotes.jpg',
  '/static/img/rescatados/Chocolate.jpg',
  '/static/img/rescatados/Luna.jpg',
  '/static/img/rescatados/Maya.jpg',
  '/static/img/rescatados/Oso.jpg',
  '/static/img/rescatados/Pexel.jpg',
  '/static/img/rescatados/Wifi.jpg',
  'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
  'https://fonts.googleapis.com/css?family=Roboto+Slab',

  '/static/js/jquery-3.3.1.js',
  '/static/js/lightbox.js',
  '/static/js/jquery.validate.js',
  '/static/js/additional-methods.js',
  '/static/js/jquery.rut.js',


  ];

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(staticCacheName).then(function(cache) {
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
      caches.match(event.request)
      .then(function(response) {
          // Cache hit - return response
          if (response) {
              return response;
          }
          return fetch(event.request);
      })
  );
});
