from django.conf.urls import url, include
from rest_framework import routers
from . import views
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name='index'),
    path('registro',views.registro, name='registro'), 
    path('acceso',views.acceso, name='acceso'),     
    path('cerrar',views.cerrar, name='cerrar'),    
    path('producto/', views.producto_ver, name="producto_ver"), 
    path('producto/new/', views.producto_crear, name="producto_crear"),     
    path('producto/<int:id>/',views.producto_eliminar, name='producto_eliminar'),
    path('producto/<int:pk>/edit/',views.producto_editar, name='producto_editar'),
    path('tienda/', views.tienda_ver, name="tienda_ver"),     
    path('tienda/new/', views.tienda_crear, name="tienda_crear"),    
    path('tienda/<int:id>/',views.tienda_eliminar, name='tienda_eliminar'), 
    path('tienda/<int:id>/ok/',views.tienda_ok, name='tienda_ok'),
    path('lista/', views.lista_ver, name="lista_ver"),
    path('lista/new/', views.lista_crear, name="lista_crear"),
    path('lista/<int:id>/',views.lista_eliminar, name='lista_eliminar'), 
    path('lista/<int:id>/edit/',views.lista_editar, name='lista_editar'),
    path('lista/<int:id>/view/',views.lista_detalle, name='lista_detalle'),
    path('lista/<int:id>/add/',views.lista_agregar, name='lista_agregar'),    
    path('lista/<int:lista>/<int:producto>',views.productolista_eliminar, name='productolista_eliminar'), 
]
