from django.contrib import admin
from .models import Persona, Producto, Tienda, Productolista

admin.site.register(Persona)
admin.site.register(Producto)
admin.site.register(Tienda)
admin.site.register(Productolista)
