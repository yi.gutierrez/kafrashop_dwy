from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.

class Persona(models.Model):

    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField(blank=False, max_length=100, primary_key=True, verbose_name='Correo Electrónico')
    run = models.CharField(max_length=12, verbose_name='Run')
    nombre_completo = models.CharField(max_length=50, verbose_name='Nombre Completo')
    fechanac = models.DateField( verbose_name='Fecha de Nacimiento')
    telefono = models.IntegerField(verbose_name='Teléfono')
    regiones = models.CharField(max_length=40, verbose_name='Región')
    comunas = models.CharField(max_length=40, verbose_name='Comuna')
    contrasenia = models.CharField(blank=False, max_length=20, default=None, verbose_name='Contraseña')
    
    def __str__(self):
        return self.email


class Tienda(models.Model):


    DISPONIBLE = 'Disponible'
    PENDIENTE = 'Pendiente'

    ESTADOS_CHOICES = (
        (DISPONIBLE, 'Disponible'),
        (PENDIENTE, 'Pendiente'),
    )

    nombre_tienda = models.CharField(max_length = 60, verbose_name='Nombre Tienda')
    nombre_sucursal = models.CharField(max_length = 60, verbose_name='Nombre Sucursal')
    direccion = models.CharField(max_length = 60, verbose_name='Direccion')
    comunas = models.CharField(max_length=40, verbose_name='Comuna')
    regiones =  models.CharField(max_length=40, verbose_name='Región')
   
    estados = models.CharField(max_length=10, choices=ESTADOS_CHOICES, default=PENDIENTE, )

    def __str__(self):
        return self.nombre_tienda


class Producto(models.Model):
    tienda = models.ForeignKey(Tienda, on_delete=models.CASCADE)
    nombre_producto = models.CharField(max_length = 60, verbose_name='Nombre')
    costo_presupuesto = models.IntegerField( verbose_name='Costo Presupuesto')
    costo_real = models.IntegerField( verbose_name='Costo Real')
    nota_adicionales =  models.CharField(max_length=40, verbose_name='Nota Adicionales')

    def __str__(self):
        return self.nombre_producto

class Lista(models.Model):


    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre_lista = models.CharField(max_length = 60, verbose_name='Nombre de Lista')
    
   
    def __str__(self):
        return self.nombre_lista


class Productolista(models.Model):
    lista = models.ForeignKey(Lista, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)


    def __str__(self):
        return self.lista.nombre_lista + " - " + self.producto.nombre_producto