# Generated by Django 2.1.3 on 2018-12-06 01:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kafra_app', '0004_auto_20181205_2156'),
    ]

    operations = [
        migrations.AddField(
            model_name='tienda',
            name='estados',
            field=models.CharField(choices=[('Aceptada', 'Aceptada'), ('Pendiente', 'Pendiente')], default='Pendiente', max_length=10),
        ),
    ]
