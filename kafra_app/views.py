from django.contrib.auth.models import User, Group
from django.shortcuts import render_to_response, redirect, render, get_object_or_404
from django.template import RequestContext
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout, login as auth_login
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Sum

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from .models import Persona, Tienda, Lista, Producto, Productolista
from .forms import PersonaForm, UsuarioForm, TiendaForm, ListaForm, ProductoForm, ProductolistaForm
from .serializers import PersonaSerializer, ProductoSerializer


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


def base_layout(request):
	return render(request,'base.html')

def index(request):
    return render(request, 'index.html', {})


def registro(request):
    if request.method == "POST":
        form = PersonaForm(request.POST)
        if form.is_valid():

            usuario = form.cleaned_data['email']
            password = form.cleaned_data['contrasenia']

            try:
                user_val = User.objects.get(
                    username=form.cleaned_data['email'])
            except User.DoesNotExist:
                user_val = None

            if user_val is None:
                user = User.objects.create_user(usuario, usuario, password)
                user.save()
                persona = form.save(commit=False)
                persona.usuario = user
                persona.save()
                return redirect('index')
        return render(request, 'registro.html', {'form': form})
    else:
        form = PersonaForm()
        return render(request, 'registro.html', {'form': form})



def acceso(request):
    if request.method == "POST":
        form = UsuarioForm(request.POST)
        user = None
        usuario = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=usuario, password=password)
        if user is not None:
            auth_login(request, user)
            return redirect('index')
    else:
        form = UsuarioForm()
    return render(request, 'acceso.html', {'form': form})


def cerrar(request):
    logout(request)
    return redirect('index')

def lista_ver(request):
    if request.user.is_superuser:
        return render(request, 'lista.html', {'lista': Lista.objects.all()})
    else:
        return render(request, 'lista.html', {'lista': Lista.objects.filter(usuario=request.user)})

def lista_crear(request):
    if request.method == "POST":
        form = ListaForm(request.POST)
        if form.is_valid():            
            lista = form.save(commit=False)
            lista.usuario = request.user
            lista.save()
            if request.user.is_superuser:
                return render(request, 'lista.html', {'lista': Lista.objects.all()})
            else:
                return render(request, 'lista.html', {'lista': Lista.objects.filter(usuario=request.user)})
        return render(request, 'lista_crear.html', {'form': form})
    else:
        form = ListaForm()
        return render(request, 'lista_crear.html', {'form': form})

def lista_editar(request, id):
    lista = get_object_or_404(Lista, pk=id)
    if request.method == "POST":
        form = ListaForm(request.POST, instance=lista)
        if form.is_valid():
            lista = form.save(commit=False)
            lista.save()
            if request.user.is_superuser:
                return render(request, 'lista.html', {'lista': Lista.objects.all()})
            else:
                return render(request, 'lista.html', {'lista': Lista.objects.filter(usuario=request.user)})
        return render(request, 'lista_crear.html', {'form': form})    
    else:
        form = ListaForm(instance=lista)
        return render(request, 'lista_crear.html', {'form': form})


def lista_eliminar(request, id):    
    lista = Lista.objects.get(pk = id)
    lista.delete()
    if request.user.is_superuser:
        return render(request, 'lista.html', {'lista': Lista.objects.all()})
    else:
        return render(request, 'lista.html', {'lista': Lista.objects.filter(usuario=request.user)})


def tienda_ok(request, id):    
    tienda = Tienda.objects.get(pk = id)
    tienda.estados = "Disponible"
    tienda.save()
    return render(request, 'tienda.html', {'tienda': Tienda.objects.all()})



def tienda_ver(request):
    return render(request, 'tienda.html', {'tienda': Tienda.objects.all()})

def tienda_crear(request):
    if request.method == "POST":
        form = TiendaForm(request.POST)
        if form.is_valid():
            tienda = form.save(commit=False)
            tienda.save()
            return render(request, 'tienda.html', {'tienda': Tienda.objects.all()})
        return render(request, 'tienda_crear.html', {'form': form})
    else:
        form = TiendaForm()
        return render(request, 'tienda_crear.html', {'form': form})


def tienda_eliminar(request, id):    
    tienda = Tienda.objects.get(pk = id)
    tienda.delete()
    return render(request, 'tienda.html', {'tienda': Tienda.objects.all()}  )


def producto_ver(request):
    return render(request, 'producto.html', {'producto': Producto.objects.all()})

def producto_crear(request):
    if request.method == "POST":
        form = ProductoForm(request.POST)
        if form.is_valid():
            producto = form.save(commit=False)
            producto.save()
            return render(request, 'producto.html', {'producto': Producto.objects.all()})
        
        form.fields["tienda"].queryset = Tienda.objects.filter(estados="Disponible")
        return render(request, 'producto_crear.html', {'form': form})
    else:
        form = ProductoForm()      
        form.fields["tienda"].queryset = Tienda.objects.filter(estados="Disponible")
        return render(request, 'producto_crear.html', {'form': form})
  


def producto_editar(request, pk):
    producto = get_object_or_404(Producto, pk=pk)
    if request.method == "POST":
        form = ProductoForm(request.POST, instance=producto)
        if form.is_valid():
            producto = form.save(commit=False)
            producto.save()
            return render(request, 'producto.html', {'producto': Producto.objects.all()}
            )
    else:
        form = ProductoForm(instance=producto)        
        form.fields["tienda"].queryset = Tienda.objects.filter(estados="Disponible")
        return render(request, 'producto_crear.html', {'form': form})

def producto_eliminar(request, id):    
    producto = Producto.objects.get(pk = id)
    producto.delete()
    return render(request, 'producto.html', {'producto': Producto.objects.all()})


def productolista_eliminar(request, lista, producto):    
    productolista = Productolista.objects.get(lista=lista, producto=producto)
    productolista.delete()

    DATA = Producto.objects.filter(productolista__lista=lista)
    DATA.count = Producto.objects.filter(productolista__lista=lista).count()
    DATA.cpres = Producto.objects.filter(productolista__lista=lista).aggregate(Sum('costo_presupuesto'))
    DATA.creal = Producto.objects.filter(productolista__lista=lista).aggregate(Sum('costo_real'))
    return render(request, 'lista_detalle.html',{'lista':Lista.objects.get(pk=lista), 'productolista':DATA})


def lista_detalle(request,id):

    DATA = Producto.objects.filter(productolista__lista=id)
    DATA.count = Producto.objects.filter(productolista__lista=id).count()
    DATA.cpres = Producto.objects.filter(productolista__lista=id).aggregate(Sum('costo_presupuesto'))
    DATA.creal = Producto.objects.filter(productolista__lista=id).aggregate(Sum('costo_real'))
    return render(request, 'lista_detalle.html', {'lista':Lista.objects.get(pk=id), 'productolista':DATA})

def lista_agregar(request,id):
    if request.method == "POST":
        form = ProductolistaForm(request.POST)
        if form.is_valid():
            producto = request.POST['producto']            
            try:                
                valida = Productolista.objects.get(lista=id, producto=producto)
            except Productolista.DoesNotExist:
                valida = None
            if valida is None:
                productolista = form.save(commit=False)
                lista = Lista.objects.get(pk = id)
                productolista.lista=lista
                productolista.save()
                DATA = Producto.objects.filter(productolista__lista=id)
                DATA.count = Producto.objects.filter(productolista__lista=id).count()
                DATA.cpres = Producto.objects.filter(productolista__lista=id).aggregate(Sum('costo_presupuesto'))
                DATA.creal = Producto.objects.filter(productolista__lista=id).aggregate(Sum('costo_real'))
                return render(request, 'lista_detalle.html',{'lista':Lista.objects.get(pk=id), 'productolista':DATA})
            else:
                return render(request, 'lista_agregar.html', {'form': form})    
            return render(request, 'lista_agregar.html', {'form': form})
    else:
        form = ProductolistaForm()        

        Q1 = Producto.objects.filter(productolista__lista=id)
        Q2 = Producto.objects.all()
        Q3 = Q2.difference(Q1) 
        form.fields["producto"].queryset = Q3
        return render(request, 'lista_agregar.html', {'form': form})
  


class PersonaViewSet(viewsets.ModelViewSet):
    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer


class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer