from django import forms
from .models import Persona, Tienda, Lista, Producto, Productolista
from django.contrib.auth.models import User


class PersonaForm(forms.ModelForm):

    class Meta:
        model = Persona
        fields = ('email', 'run', 'nombre_completo', 'fechanac',
                  'telefono', 'regiones', 'comunas', 'contrasenia')
        labels = {
            'email': 'Correo Electrónico',
            'run': 'Run',
            'nombre_completo': 'Nombre Completo',
            'fechanac': 'Fecha de Nacimiento',
            'telefono': 'Teléfono',
            'regiones': 'Región',
            'comunas': 'Comuna',
            'contrasenia': 'Contraseña'
        }
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'run': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre_completo': forms.TextInput(attrs={'class': 'form-control'}),
            'fechanac': forms.DateInput(attrs={'type': 'date'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'regiones': forms.Select(attrs={'class': 'form-control', 'id': 'regiones'}),
            'comunas': forms.Select(attrs={'class': 'form-control', 'id': 'comunas'}),
            'contrasenia': forms.TextInput(attrs={'class': 'form-control'})
        }


class UsuarioForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'password',)


class TiendaForm(forms.ModelForm):
    class Meta:
        model = Tienda
        fields = ('nombre_tienda', 'nombre_sucursal',
                  'direccion',  'regiones', 'comunas',)

        labels = {
            'nombre_tienda': 'Nombre Tienda',
            'nombre_sucursal': 'Nombre Sucursal',
            'direccion': 'Dirección',
            'regiones': 'Región',
            'comunas': 'Comuna',
        }
        widgets = {
            'nombre_tienda': forms.TextInput(attrs={'class': 'form-control', 'id': 'nombre_tienda'}),
            'nombre_sucursal': forms.TextInput(attrs={'class': 'form-control', 'id': 'nombre_sucursal'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control', 'id': 'direccion'}),
            'regiones': forms.Select(attrs={'class': 'form-control', 'id': 'regiones'}),
            'comunas': forms.Select(attrs={'class': 'form-control', 'id': 'comunas'}),
        }



class ListaForm(forms.ModelForm):
    class Meta:
        model = Lista        
        fields = ('nombre_lista',)        
        labels = {'nombre_lista': 'Nombre Lista',}        
        widgets = { 'nombre_lista': forms.TextInput(attrs={'class': 'form-control', 'id': 'nombre_lista'}),
        }

class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ('nombre_producto','costo_presupuesto','costo_real','tienda','nota_adicionales')

        labels = {
            'nombre_producto':'Nombre',
            'costo_presupuesto':'Costo Presupuesto',
            'costo_real':'Costo Real',
            'tienda':'Tienda',
            'nota_adicionales':'Nota Adicionales',          

            }
        widgets= {
            'nombre_producto': forms.TextInput(attrs={'class':'form-control','id':'nombre_producto'}),
            'costo_presupuesto': forms.TextInput(attrs={'class':'form-control','id':'costo_presupuesto'}),
            'costo_real': forms.TextInput(attrs={'class':'form-control','id':'costo_real'}),
            'nota_adicionales':forms.TextInput(attrs={'class':'form-control','id':'nota_adicionales'}),
        	'tienda': forms.Select(attrs={'class':'form-control','id':'tienda'}),
        }

        
class ProductolistaForm(forms.ModelForm):
    class Meta:
        model = Productolista
        fields = ('producto',)
        labels = {
            'producto':'Producto',  
            }
        widgets= {
        	'producto': forms.Select(attrs={'class':'form-control','id':'producto'}),
        }